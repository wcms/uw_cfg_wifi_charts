<?php

/**
 * @file
 * uw_cfg_wifi_charts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_wifi_charts_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
