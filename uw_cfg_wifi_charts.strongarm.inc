<?php

/**
 * @file
 * uw_cfg_wifi_charts.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_wifi_charts_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_wifi_charts_num_days_to_display_data';
  $strongarm->value = '7';
  $export['uw_wifi_charts_num_days_to_display_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_wifi_charts_num_days_to_display_data_cover_page';
  $strongarm->value = '1';
  $export['uw_wifi_charts_num_days_to_display_data_cover_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_wifi_charts_rogues_enabled';
  $strongarm->value = 1;
  $export['uw_wifi_charts_rogues_enabled'] = $strongarm;

  return $export;
}
