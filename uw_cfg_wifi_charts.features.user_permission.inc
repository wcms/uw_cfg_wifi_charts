<?php

/**
 * @file
 * uw_cfg_wifi_charts.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_wifi_charts_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uwaterloo wifi charts'.
  $permissions['administer uwaterloo wifi charts'] = array(
    'name' => 'administer uwaterloo wifi charts',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'uw_wifi_charts',
  );

  return $permissions;
}
